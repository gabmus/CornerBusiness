---
{"title": "La diagnosi ortodontica: rispondere a 250 quesiti", "permalink": "La_diagnosi_ortodontica__rispondere_a_250_quesiti", "datetime": "2018-08-01T13:18:40", "tags": ["ortodonzia"], "picture": "https://lh4.googleusercontent.com/-gkxWONCddK0/AAAAAAAAAAI/AAAAAAAAAKw/L3fGAXOct_g/photo.jpg"}
---
Non può esservi terapia ortodontica per adulti o bambini se prima non viene effettuata una diagnosi.

Questo è un momento delicato che deve essere supportato da una competenza sufficiente ad affrontare i problemi senza crearne dei nuovi.

Una diagnosi completa, pur se perfettibile, deve implicare la risposta a circa 250 quesiti che permettono di inquadrare il problema del paziente, piccolo o grande, all'interno delle possibilità terapeutiche che offre la moderna Ortognatodonzia.

In questo articolo saranno esposte in maniera semplificata le principali tappe dell'iter diagnostico che, secondo le attuali tendenze medico-scientifiche, sarà "orientato ai problemi".