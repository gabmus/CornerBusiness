#!/usr/bin/env python3

import json
import os

ARTICLES_PATH='./blocks'
DEST_ARTICLES_INDEX='./blocks_data.json'

articles_list=os.listdir(ARTICLES_PATH)
articles_list.sort()
print(articles_list)
articles=[]

for article in articles_list:
    if article[-3:] == '.md':
        article_s = ''
        with open('{0}/{1}'.format(ARTICLES_PATH, article)) as fd:
            article_s = fd.read()
        in_meta=0
        meta_s=""
        article_nometa=""
        for line in article_s.split('\n'):
            if line=='---' and in_meta==0:
                in_meta=1
            elif line=='---' and in_meta==1:
                in_meta=2    
            elif in_meta==1:
                meta_s+='{0}\n'.format(line)
            elif in_meta==2:
                article_nometa+='{0}\n'.format(line)
        meta=json.loads(meta_s)
        meta['content'] = article_nometa
        articles.append(meta)
    else:
        print('WARNING: file {0} skipped because missing .md extension')

articles_s = json.dumps(articles)
with open(DEST_ARTICLES_INDEX, "w+") as fd:
    fd.write(articles_s)


