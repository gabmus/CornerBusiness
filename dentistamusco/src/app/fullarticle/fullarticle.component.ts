import { Component, OnInit } from '@angular/core';
import { DatafetcherService } from '../datafetcher.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-fullarticle',
    templateUrl: './fullarticle.component.html',
    styleUrls: ['./fullarticle.component.css']
})
export class FullarticleComponent implements OnInit {

	public permalink$: string;
	public meta: any;
	public article: string;

    constructor(private datafetcher: DatafetcherService, private route: ActivatedRoute) {
		this.route.params.subscribe(
            params => this.permalink$ = params.permalink
        );
	}

    ngOnInit() {
		this.datafetcher.getArticleData(this.permalink$).subscribe(
            data => {
				let meta_s="";
				let in_meta=0;
				let article_nometa="";
				data.split('\n').map((line, index) => {
					if (line==='---' && in_meta===0) in_meta=1;
					else if (line==='---' && in_meta===1) in_meta=2;
					else if (in_meta===1) meta_s += `${line}\n`;
					else if (in_meta===2) article_nometa += `${line}\n`;
				});
				this.meta = JSON.parse(meta_s);
				this.article = article_nometa;
			}
        );
    }

}
