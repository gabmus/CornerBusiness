import { Component, OnInit } from '@angular/core';
import { DatafetcherService } from '../datafetcher.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-blogpreview',
    templateUrl: './blogpreview.component.html',
    styleUrls: ['./blogpreview.component.css']
})
export class BlogpreviewComponent implements OnInit {

    blogdata$: Object;

    constructor(private datafetcher: DatafetcherService) { }

    ngOnInit() {
        this.datafetcher.getBlogpreviewData().subscribe(
            data => {
                this.blogdata$ = data;
                /*if (this.blogdata$.length > 4)
                    this.blogdata$ = this.blogdata$.slice(0, 4);*/
            }
        );
    }

}
