import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogpreviewComponent } from './blogpreview.component';

describe('BlogpreviewComponent', () => {
  let component: BlogpreviewComponent;
  let fixture: ComponentFixture<BlogpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
