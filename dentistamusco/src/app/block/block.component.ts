import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-block',
    templateUrl: './block.component.html',
    styleUrls: ['./block.component.css']
})
export class BlockComponent implements OnInit {
    
    @Input() public title: string;
    @Input() public content: string;
    @Input() public picture: string;

    constructor() { }

    ngOnInit() {
        if (!this.title || this.title.length === 0)
            this.title = "No title";

        if (!this.content || this.content .length === 0)
            this.content = "No title";
    }

}
