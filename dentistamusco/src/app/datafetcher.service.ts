import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatafetcherService {

    constructor(private http: HttpClient) { }

    getBlocksData() {
        return this.http.get('/assets/data/blocks_data.json');
    }

    getBlogpreviewData() { // this method must return a json like follows
        /*
            [
                {
                    "title": "Article Title",
                    "content": "Article content in **Markdown**.",
                    "permalink": "article-title",
                    // datetime as output of `date --iso-8601=seconds`
                    "datetime": "2018-07-11T11:47:38+02:00",
                    "tags": ["list", "of", "tags"],
                    "picture": "https://article_picture_link.jpg"
                }, ...
            ]
         */
        return this.http.get('/assets/data/articles_index.json');
    }

    getArticleData(permalink: any) {
        return this.http.get(`/assets/data/articles/${permalink}.md`, {responseType: 'text'});
    }
}
