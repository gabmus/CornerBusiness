import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-videobackground',
    templateUrl: './videobackground.component.html',
    styleUrls: ['./videobackground.component.css']
})
export class VideobackgroundComponent implements OnInit {
    
    videoTag;

    constructor(private sanitizer: DomSanitizer) {
        this.videoTag = this.getVideoTag();
    }

    private getVideoTag() {
        return this.sanitizer.bypassSecurityTrustHtml(
            `<video autoplay="autoplay" muted loop="loop" style="position: absolute; right: 50% bottom: 50%; transform: translate(-50%, 0%); min-width: 100%; min-height: 100%; z-index: -1; overflow: hidden; height: 100vh;" id="videobg">
                <source src="/assets/videobg.webm" type="video/webm">
                <source src="/assets/videobg.mp4" type="video/mp4">
            </video>`
        );
    }

    ngOnInit() {
    }

}
