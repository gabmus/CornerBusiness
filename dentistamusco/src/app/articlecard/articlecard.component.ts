import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-articlecard',
    templateUrl: './articlecard.component.html',
    styleUrls: ['./articlecard.component.css']
})
export class ArticlecardComponent implements OnInit {

    @Input() public title: string;
    @Input() public preview: boolean;
    @Input() public permalink: string;
    @Input() public content: string;
    @Input() public picture: string;
    @Input() public datetime: string;

    public actualDate: Date;

    constructor() { }

    ngOnInit() {
        if (!this.title || this.title.length === 0)
            this.title = "No title";
        
        if (!this.permalink || this.permalink.length === 0)
            this.permalink = this.title.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"").replace(/\s{2,}/g," ").replace(/ /g, "_"); // if no permalink, creates one by transforming the title to lowercase, removing punctuation, removing doulbe spaces and replacing spaces with uderscores

        if (!this.content || this.content .length === 0)
            this.content = "No content";

        if (this.datetime)
            this.actualDate = new Date(this.datetime);

        if (this.preview === undefined)
            this.preview = true;
    }

}
