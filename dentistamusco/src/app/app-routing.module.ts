import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageComponent } from './homepage/homepage.component'; 
import { ContactComponent } from './contact/contact.component';
import { TrainingComponent } from './training/training.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BlogComponent } from './blog/blog.component'; 
import { FullarticleComponent } from './fullarticle/fullarticle.component'; 

const routes: Routes = [
    {
        path: '',
        component: HomepageComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'training',
        component: TrainingComponent
    },
    {
        path: 'blog',
        component: BlogComponent
    },
    {
        path: 'blog/article/:permalink',
        component: FullarticleComponent
    },
    {
        path: '404',
        component: NotfoundComponent
    },
    { // WARNING: ALWAYS KEEP THIS LAST! Any path under this won't be parsed
        path: '**',
        redirectTo: '404'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
