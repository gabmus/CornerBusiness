import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MarkdownModule } from 'ngx-markdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BlockviewComponent } from './blockview/blockview.component';
import { VideobackgroundComponent } from './videobackground/videobackground.component';
import { HomepageComponent } from './homepage/homepage.component';
import { BlockComponent } from './block/block.component';

import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'angular-custom-modal';
import { ContactComponent } from './contact/contact.component';
import { BlogpreviewComponent } from './blogpreview/blogpreview.component';
import { ArticlecardComponent } from './articlecard/articlecard.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BlogComponent } from './blog/blog.component';
import { FullarticleComponent } from './fullarticle/fullarticle.component';
import { FooterComponent } from './footer/footer.component';
import { TrainingComponent } from './training/training.component';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        BlockviewComponent,
        VideobackgroundComponent,
        HomepageComponent,
        BlockComponent,
        ContactComponent,
        BlogpreviewComponent,
        ArticlecardComponent,
        NotfoundComponent,
        BlogComponent,
        FullarticleComponent,
        FooterComponent,
        TrainingComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ModalModule,
        MarkdownModule.forRoot(),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
