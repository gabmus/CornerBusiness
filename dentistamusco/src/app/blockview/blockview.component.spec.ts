import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockviewComponent } from './blockview.component';

describe('BlockviewComponent', () => {
  let component: BlockviewComponent;
  let fixture: ComponentFixture<BlockviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
