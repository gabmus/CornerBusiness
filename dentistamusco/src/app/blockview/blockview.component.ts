import { Component, OnInit } from '@angular/core';
import { DatafetcherService } from '../datafetcher.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-blockview',
    templateUrl: './blockview.component.html',
    styleUrls: ['./blockview.component.css']
})
export class BlockviewComponent implements OnInit {

    blocksdata$: Object;

    constructor(private datafetcher: DatafetcherService) { }

    ngOnInit() {
        this.datafetcher.getBlocksData().subscribe(
            data => this.blocksdata$ = data
        );
    }

}
