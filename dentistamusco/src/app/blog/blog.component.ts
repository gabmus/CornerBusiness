import { Component, OnInit } from '@angular/core';
import { DatafetcherService } from '../datafetcher.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
    
    articleslist$: Object;

    constructor(private datafetcher: DatafetcherService) { }

    ngOnInit() {
        this.datafetcher.getBlogpreviewData().subscribe(
            data => {
                this.articleslist$ = data;
                /*if (this.blogdata$.length > 4)
                    this.blogdata$ = this.blogdata$.slice(0, 4);*/
            }
        );
    }
}
