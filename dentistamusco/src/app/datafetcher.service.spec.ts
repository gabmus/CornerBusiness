import { TestBed, inject } from '@angular/core/testing';

import { DatafetcherService } from './datafetcher.service';

describe('DatafetcherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatafetcherService]
    });
  });

  it('should be created', inject([DatafetcherService], (service: DatafetcherService) => {
    expect(service).toBeTruthy();
  }));
});
