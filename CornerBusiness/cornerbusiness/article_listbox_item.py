import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Gio, GdkPixbuf
# from . import threading_helper as ThreadingHelper
import os

class ArticleBox(Gtk.ListBoxRow):
    def __init__(self, article, *args, **kwds):
        super().__init__(*args, **kwds)

        self.article = article

        #self.set_halign(Gtk.Align.CENTER)
        #self.set_valign(Gtk.Align.CENTER)

        self.container_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.label = Gtk.Label()
        self.label.set_text(self.article['title'])
        self.label.set_halign(Gtk.Align.START)
        self.label.set_line_wrap(True)

        self.container_box.pack_start(self.label, False, True, 3)
        self.container_box.set_margin_left(12)
        self.container_box.set_margin_right(12)
        self.container_box.set_margin_top(6)
        self.container_box.set_margin_bottom(6)
        self.add(self.container_box)
        self.set_size_request(200, -1)
