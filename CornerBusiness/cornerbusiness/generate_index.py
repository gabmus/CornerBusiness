
import os
import json
import datetime
import pytz

RSS_PREFIX='''<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Dentista Carmelo Musco</title>
    <link>https://dentistamusco.it/</link>
    <description>Blog del Dr. Carmelo Musco</description>
    <generator>CornerBusiness - gitlab.com/gabmus/cornerbusiness</generator>
    <language>it-IT</language>
    <atom:link href="https://techpills.technology/index.xml" rel="self" type="application/rss+xml" />'''

RSS_SUFFIX='''</channel>
</rss>'''

def _get_time():
    return f"{datetime.datetime.now(pytz.timezone('Europe/Rome')):%a, %d %b %Y %H:%M:%S %z}"

def _format_datetime(datetime):
    return f"{datetime.datetime.fromisoformat(datetime):%a, %d %b %Y %H:%M:%S %z}"

def _article2rss(article):
    return '''<item>
        <title>{0}</title>
        <link>https://dentistamusco.it/blog/article/{1}</link>
        <pubDate>{2}+0200</pubDate>

        <guid>https://dentistamusco.it/blog/article/{1}</guid>
        <description>{3}</description>
    </item>
    '''.format(
        article['title'],
        article['permalink'],
        _format_datetime(article['datetime']),
        article['content']
    )

def generate_rss(articles, dest_rss_path):
    rss_s = RSS_PREFIX
    rss_s += '<lastBuildDate>{0}</lastBuildDate>'.format(_get_time())

    for article in articles:
        rss_s += _article2rss(article)

    rss_s += RSS_SUFFIX

    with open(dest_rss_path, "w+") as fd:
        fd.write(rss_s)

def generate_index(articles_path, dest_index_path, dest_rss_path):
    articles_list=os.listdir(articles_path)
    articles=[]

    for article in articles_list:
        if article[-3:] == '.md':
            article_s = ''
            with open('{0}/{1}'.format(articles_path, article)) as fd:
                article_s = fd.read()
            in_meta=0
            meta_s=""
            article_nometa=""
            for line in article_s.split('\n'):
                if line=='---' and in_meta==0:
                    in_meta=1
                elif line=='---' and in_meta==1:
                    in_meta=2    
                elif in_meta==1:
                    meta_s+='{0}\n'.format(line)
                elif in_meta==2:
                    article_nometa+='{0}\n'.format(line)
            meta=json.loads(meta_s)
            meta['content'] = '{0}...'.format(article_nometa[:50])
            meta['permalink'] = article[:-3] # removes .md
            articles.append(meta)
        else:
            print('WARNING: file {0} skipped because missing .md extension'.format(article))

    articles = sorted(articles, key=lambda k: k['datetime'], reverse=True)

    generate_rss(articles, dest_rss_path)

    articles_s = json.dumps(articles)
    with open(dest_index_path, "w+") as fd:
        fd.write(articles_s)
