import os
from git import Repo
import pathlib

class RepoManager:

    def __init__(self, repo_path):
        if not os.path.isdir(repo_path):
            raise ValueError('ERROR: The path provided {0} is not a directory'.format(repo_path))
            return None
        if not os.path.isdir('{0}/.git'.format(repo_path)):
            raise ValueError('ERROR: The path provided {0} is not a git repository'.format(repo_path))
            return None
        self.repo_path = repo_path
        self.repo = Repo(self.repo_path)

    def commit(self):
        untracked_files = self.repo.untracked_files
        commit_msg = 'Added articles'
        for f in untracked_files:
            f_name = pathlib.PurePath(f).name
            if f_name[-3:] == '.md':
                commit_msg += ' {0}'.format(
                    f_name
                )
        if commit_msg == 'Added articles':
            print('Warning: No articles added, skipping')
            return
        self.repo.git.add('*')
        self.repo.git.commit('-m "{0}"'.format(commit_msg))
        self.repo.git.push()
        
