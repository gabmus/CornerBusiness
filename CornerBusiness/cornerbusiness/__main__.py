# __main__.py
#
# Copyright (C) 2017 GabMus
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import pathlib
import json
import datetime
import re
import requests

IMG_EXTENSIONS = [
    '.jpg',
    '.jpeg',
    '.png',
    '.svg'
]

def image_url_exists(path):
    if not path or path[:4].lower() != 'http':
        return False
    r = requests.head(path)
    return (path[-4:].lower() in IMG_EXTENSIONS or path[-5:].lower() in IMG_EXTENSIONS) and r.status_code == requests.codes.ok


import argparse
from gi.repository import Gtk, GObject, GtkSource, Gdk, Gio, GdkPixbuf

from . import listbox_helper as ListboxHelper
from . import articles_helper as ArticlesHelper
from . import article_listbox_item as ArticleListboxItem
from . import repo_manager as RepoManager

HOME = os.environ.get('HOME')
G_CONFIG_FILE_PATH = '{0}/.config/cornerbusiness.json'.format(HOME)
G_CACHE_PATH = '{0}/.cache/cornerbusiness/'.format(HOME)
DEFAULT_REPO_PATH = 'git/cornerbusiness_assets'
ARTICLES_PATH = 'data/articles'
INDEX_PATH = 'data/articles_index.json'
RSS_PATH = 'data/feed.xml'

ISO_DATE_REGEX = re.compile(r'^(?P<full>((?P<year>\d{4})([/-]?(?P<mon>(0[1-9])|(1[012]))([/-]?(?P<mday>(0[1-9])|([12]\d)|(3[01])))?)?(?:T(?P<hour>([01][0-9])|(?:2[0123]))(\:?(?P<min>[0-5][0-9])(\:?(?P<sec>[0-5][0-9]([\,\.]\d{1,10})?))?)?(?:Z|([\-+](?:([01][0-9])|(?:2[0123]))(\:?(?:[0-5][0-9]))?))?)?))$')

# check if inside flatpak sandbox. if so change some variables
if 'XDG_RUNTIME_DIR' in os.environ.keys():
    if os.path.isfile('{0}/flatpak-info'.format(os.environ['XDG_RUNTIME_DIR'])):
        G_CONFIG_FILE_PATH = '{0}/cornerbusiness.json'.format(os.environ.get('XDG_CONFIG_HOME'))
        G_CACHE_PATH = '{0}/cornerbusiness/'.format(os.environ.get('XDG_CACHE_HOME'))

if not os.path.isdir(G_CACHE_PATH):
    os.makedirs(G_CACHE_PATH)


class Application(Gtk.Application):
    def __init__(self, **kwargs):
        GObject.type_register(GtkSource.View)
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/cornerbusiness/ui/ui.glade'
        )
        super().__init__(
            application_id='org.gabmus.cornerbusiness',
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs
        )
        self.RESOURCE_PATH = '/org/gabmus/cornerbusiness/'

        self.CONFIG_FILE_PATH = G_CONFIG_FILE_PATH  # G stands for Global (variable)

        self.configuration = self.get_config_file()

        self.my_repo_manager = RepoManager.RepoManager(self.configuration['repopath'])

        self.builder.connect_signals(self)

        settings = Gtk.Settings.get_default()
        # settings.set_property("gtk-application-prefer-dark-theme", True)

        self.window = self.builder.get_object('window')

        self.window.set_icon_name('org.gabmus.cornerbusiness')

        self.window.resize(
            self.configuration['windowsize']['width'],
            self.configuration['windowsize']['height']
        )

        self.errorDialog = Gtk.MessageDialog()
        self.errorDialog.add_button('Ok', 0)
        self.errorDialog.set_default_response(0)
        self.errorDialog.set_transient_for(self.window)

        self.articles = []

        self.articles_listbox = self.builder.get_object('articlesListbox')

        self.title_entry = self.builder.get_object('titleEntry')
        self.permalink_entry = self.builder.get_object('permalinkEntry')
        self.datetime_entry = self.builder.get_object('datetimeEntry')
        self.tags_entry = self.builder.get_object('tagsEntry')
        self.picture_url_entry = self.builder.get_object('pictureUrlEntry')
        self.body_textbuffer = GtkSource.Buffer()
        self.lang_manager = GtkSource.LanguageManager()
        self.body_textbuffer.set_language(self.lang_manager.get_language('markdown'))
        self.body_textview_container = self.builder.get_object('bodyTextViewContainer')
        self.body_textview = GtkSource.View.new_with_buffer(self.body_textbuffer)

        self.body_textview.set_show_line_numbers(True)
        self.body_textview.set_indent_on_tab(True)
        self.body_textview.set_tab_width(4)
        self.body_textview.set_indent_width(4)
        self.body_textview.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self.body_textview.set_highlight_current_line(True)
        self.body_textview.set_monospace(True)

        self.body_textview_container.add(self.body_textview)
        self.set_datetime_button = self.builder.get_object('setDatetimeButton')
        self.error_infobar = self.builder.get_object('errorInfobar')
        self.error_infobar_label = self.builder.get_object('errorInfobarLabel')
        self.mode_label = self.builder.get_object('modeLabel')
        self.delete_article_button = self.builder.get_object('deleteArticleButton')

        self.overwrite_infobar = self.builder.get_object('overwriteInfobar')

        self.current_article_text = ''
        self.current_article_permalink = ''

    def on_window_size_allocate(self, *args):
        alloc = self.window.get_allocation()
        self.configuration['windowsize']['width'] = alloc.width
        self.configuration['windowsize']['height'] = alloc.height

    def do_before_quit(self):
        self.save_config_file()

    def save_config_file(self, n_config=None):
        if not n_config:
            n_config = self.configuration
        with open(self.CONFIG_FILE_PATH, 'w') as fd:
            fd.write(json.dumps(n_config))
            fd.close()

    def get_config_file(self):
        if not os.path.isfile(self.CONFIG_FILE_PATH):
            n_config = {
                'windowsize': {
                    'width': 600,
                    'height': 400
                },
                'repopath': '{0}/{1}'.format(HOME, DEFAULT_REPO_PATH)
            }
            self.save_config_file(n_config)
            return n_config
        else:
            do_save = False
            with open(self.CONFIG_FILE_PATH, 'r') as fd:
                config = json.loads(fd.read())
                fd.close()
                if not 'windowsize' in config.keys():
                    config['windowsize'] = {
                        'width': 600,
                        'height': 400
                    }
                    do_save = True
                if not 'repopath' in config.keys():
                    config['repopath'] = '{0}/{1}'.format(HOME, DEFAULT_REPO_PATH)
                    do_save = True
                if do_save:
                    self.save_config_file(config)
                return config

    def do_activate(self):
        self.add_window(self.window)
        self.window.set_wmclass('CornerBusiness', 'CornerBusiness')

        appMenu = Gio.Menu()
        appMenu.append("About", "app.about")
        appMenu.append("Settings", "app.settings")
        appMenu.append("Quit", "app.quit")

        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.on_about_activate)
        self.builder.get_object("aboutdialog").connect(
            "delete-event", lambda *_:
                self.builder.get_object("aboutdialog").hide() or True
        )
        self.add_action(about_action)

        settings_action = Gio.SimpleAction.new("settings", None)
        settings_action.connect("activate", self.on_settings_activate)
        self.builder.get_object("settingsWindow").connect(
            "delete-event", lambda *_:
                self.builder.get_object("settingsWindow").hide() or True
        )
        self.add_action(settings_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_quit_activate)
        self.add_action(quit_action)
        self.set_app_menu(appMenu)

        self.window.show_all()

        # After components init, do the following
        self.refresh_articles_list()

    def do_command_line(self, args):
        """
        GTK.Application command line handler
        called if Gio.ApplicationFlags.HANDLES_COMMAND_LINE is set.
        must call the self.do_activate() to get the application up and running.
        """
        Gtk.Application.do_command_line(self, args)  # call the default commandline handler
        # make a command line parser
        parser = argparse.ArgumentParser(prog='gui')
        # add a -c/--color option
        parser.add_argument('-q', '--quit-after-init', dest='quit_after_init', action='store_true', help='initialize application and quit')
        # parse the command line stored in args, but skip the first element (the filename)
        self.args = parser.parse_args(args.get_arguments()[1:])
        # call the main program do_activate() to start up the app
        self.do_activate()
        return 0

    def on_about_activate(self, *args):
        self.builder.get_object("aboutdialog").show()

    def on_settings_activate(self, *args):
        self.builder.get_object("settingsWindow").show()

    def on_quit_activate(self, *args):
        self.do_before_quit()
        self.quit()

    def onDeleteWindow(self, *args):
        self.do_before_quit()
        self.quit()

    def on_aboutdialog_close(self, *args):
        self.builder.get_object("aboutdialog").hide()

    def title_to_permalink(self, title):
        return re.sub('[^A-Za-z0-9]', '_', title.strip())
    
    def on_setDatetimeButton_clicked(self, btn):
        current_datetime = datetime.datetime.now().replace(microsecond=0).isoformat()
        self.datetime_entry.set_text(current_datetime)

    def on_titleEntry_changed(self, entry):
        self.permalink_entry.set_text(
            self.title_to_permalink(entry.get_text())
        )

    def refresh_articles_list(self):
        self.articles = ArticlesHelper.parse_articles_dir(
            '{0}/{1}'.format(
                self.configuration['repopath'],
                ARTICLES_PATH
            )
        )
        ListboxHelper.empty_listbox(self.articles_listbox)
        for article in self.articles:
            item = ArticleListboxItem.ArticleBox(article)
            self.articles_listbox.add(item)
        self.articles_listbox.show_all()


    def format_article(self, title, permalink, datetime, picture_url, tags, body):
        meta = {
            'title': title,
            'permalink': permalink,
            'datetime': datetime,
            'tags': tags,
            'picture': picture_url
        }

        text = '---\n{0}\n---\n{1}'.format(
            json.dumps(meta),
            body
        )

        return text

    def save_article(self, text, permalink, overwrite=False):
        self.current_article_permalink = permalink
        self.current_article_text = text
        if ArticlesHelper.write_article(
            f_content = text,
            path = '{0}/{1}'.format(
                self.configuration['repopath'],
                ARTICLES_PATH
            ),
            permalink = permalink,
            index_path = '{0}/{1}'.format(
                self.configuration['repopath'],
                INDEX_PATH
            ),
            overwrite = overwrite
        ):
            self.refresh_articles_list()
        else:
            self.overwrite_infobar.show()

    def on_overwriteButtonYes_clicked(self, btn):
        self.save_article(
            text = self.current_article_text,
            permalink = self.current_article_permalink,
            overwrite = True
        )
        self.overwrite_infobar.hide()

    def on_refreshArticlesButton_clicked(self, btn):
        self.refresh_articles_list()

    def on_newArticleButton_clicked(self, btn):
        self.title_entry.set_text('')
        self.permalink_entry.set_text('')
        self.datetime_entry.set_text('')
        self.tags_entry.set_text('')
        self.picture_url_entry.set_text('')
        self.body_textbuffer.set_text('')

    def on_overwriteButtonNo_clicked(self, btn):
        self.overwrite_infobar.hide()

    def on_saveButton_clicked(self, btn):
        # first validate, then proceed to saving
        can_save = True
        
        error_string = ''
        title = self.title_entry.get_text().strip()
        permalink = self.permalink_entry.get_text()
        datetime = self.datetime_entry.get_text()
        picture_url = self.picture_url_entry.get_text()
        tags = [t.strip() for t in self.tags_entry.get_text().strip().split(',')]
        body = self.body_textbuffer.get_text(
            self.body_textbuffer.get_start_iter(),
            self.body_textbuffer.get_end_iter(),
            True
        )
        if not title or title == '':
            can_save = False
            error_string += 'Title is invalid\n'
        if not permalink or permalink == '':
            can_save = False
            error_string += 'Permalink is invalid\n'
        if not ISO_DATE_REGEX.match(datetime):
            can_save = False
            error_string += 'Date & time is invalid. Try pressing the button to re-generate it\n'
        if not image_url_exists(picture_url):
            can_save = False
            error_string += 'Picture url is invalid. Try and see if the link works and if it points to a picture\n'

        if can_save:
            self.error_infobar.hide()
            self.save_article(self.format_article(
                title = title,
                permalink = permalink,
                datetime = datetime,
                picture_url = picture_url,
                tags = tags,
                body = body
            ), permalink)
        else:
            print('Validation failed')
            self.error_infobar_label.set_text(error_string)
            self.error_infobar.show()

    def on_pushButton_clicked(self, btn):
        self.my_repo_manager.commit()

    def on_deleteArticleButton_clicked(self, btn):
        dialog = self.builder.get_object('deleteArticleMessageDialog')
        dialog.set_markup('<b>Are you sure?</b>\nDo you really want to delete the article <i>"{0}"</i>?'.format(self.title_entry.get_text()))
        dialog.run()

    def on_deleteArticleMessageDialog_response(self, dialog, response):
        YES_RESPONSE=-8
        NO_RESPONSE=-9
        
        if response == YES_RESPONSE:
            self.delete_article(self.current_article_permalink)
        dialog.hide()

    def delete_article(self, permalink):
        ArticlesHelper.delete_article(
            '{0}/{1}/{2}.md'.format(
                self.configuration['repopath'],
                ARTICLES_PATH,
                permalink
            )
        )
        ArticlesHelper.generate_index(
            '{0}/{1}'.format(self.configuration['repopath'], ARTICLES_PATH),
            '{0}/{1}'.format(
                self.configuration['repopath'],
                INDEX_PATH
            ),
            '{0}/{1}'.format(
                self.configuration['repopath'],
                RSS_PATH
            )
        )
        self.refresh_articles_list()
        self.on_newArticleButton_clicked(None)

    def on_articlesListbox_row_selected(self, listbox, item):
        if not item:
            self.current_article_permalink = ''
            self.current_article_text = ''
            self.delete_article_button.set_sensitive(False)
            return
        self.current_article_permalink = item.article['permalink']
        self.current_article_text = item.article['body']
        self.delete_article_button.set_sensitive(True)
        self.title_entry.set_text(item.article['title'])
        self.datetime_entry.set_text(item.article['datetime'])
        self.tags_entry.set_text(
            ','.join(item.article['tags'])
        )
        self.picture_url_entry.set_text(item.article['picture'])
        self.body_textbuffer.set_text(item.article['body'])

    def on_errorInfobar_response(self, infobar, response_id):
        infobar.hide()

def main():
    application = Application()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
