import os
import json

from . import generate_index as GenerateIndex

def parse_article(path):
    if path[-3:] != '.md':
        print('WARNING: file {0} skipped because missing .md extension'.format(path))
        return None

    article_s = ''
    with open(path) as fd:
        article_s = fd.read()
    in_meta=0
    meta_s=""
    article_nometa=""
    for line in article_s.split('\n'):
        if line=='---' and in_meta==0:
            in_meta=1
        elif line=='---' and in_meta==1:
            in_meta=2    
        elif in_meta==1:
            meta_s+='{0}\n'.format(line)
        elif in_meta==2:
            article_nometa+='{0}\n'.format(line)
    meta=json.loads(meta_s)
    meta['body'] = article_nometa
    #meta['permalink'] = path[:-3] # removes .md
    return meta

def parse_articles_dir(dir_path):
    articles_list=os.listdir(dir_path)
    articles=[]

    for article in articles_list:
        articles.append(parse_article('{0}/{1}'.format(dir_path, article)))

    articles = sorted(articles, key=lambda k: k['datetime'], reverse=True)
    return articles

def write_article(f_content, path, permalink, index_path, overwrite=False):
    if type(f_content) != str:
        raise ValueError('f_content must be a string!')
        return False
    f_path = '{0}/{1}.md'.format(path, permalink)
    if not overwrite and os.path.isfile(f_path):
        print('Note: trying to overwrite existing file, but overwrite is False')
        return False

    with open(f_path, 'w+') as fd:
        fd.write(f_content)

    GenerateIndex.generate_index(path, index_path)
    return True

def generate_index(articles_path, dest_index_path):
    GenerateIndex.generate_index(articles_path, dest_index_path)

def delete_article(path):
    print(path)
    if os.path.isfile(path) and path[-3:] == '.md':
        os.remove(path)
    else:
        print('ERROR: trying to remove an article that is either not an article or doesn\'t exist')
